#Creacion de un proyecto en express

##Paso 0: solo CONTI proxy para npm y git

```
npm config set https-proxy http://172.16.57.4:8080

git config --global http.proxy http://172.16.57.4:8080
```
##Paso 1: Creamos una carpeta
##Paso 2: abrimos la carpeta con VS CODE
##Paso 3: abrimos la terminal integrada (nos debe llevar a la carpeta de trabajo)
##Paso 4: inicializamos un proyecto de nodes
```
npm init
```
##Paso 5: Instalamos las dependencias
```
npm install express
npm install jade
```
##Paso 6: Creamos un archivo de servidor "server.js"