const express = require('express');
const router = express.Router();

const homeController = require('../controllers/home.controller');

router.get('/', homeController.index);

// ==================================================
router.get('/contactenos', homeController.contactenos);

router.get('/misionVision', homeController.misionVision);

// ====================================================

module.exports = router;